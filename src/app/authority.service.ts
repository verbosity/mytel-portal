import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Authority } from './authority';
import { map } from 'rxjs/operators';

@Injectable()

// Service for authorities data.
export class AuthorityService {

    // We need Http to talk to a remote server.
    constructor(private _http: Http) { }

    // Get list of authorities from remote server.
    readAuthorities(): Observable<Authority[]> {

        return this._http
            .get('http://localhost/api/authority/read.php')
            .pipe(map((res: Response) => res.json()));
    }

    // Send authority data to remote server to create it.
    createAuthority(authority): Observable<Authority> {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/authority/create.php',
            authority,
            options
        ).pipe(map((res: Response) => res.json()));
    }

    // Get a authority details from remote server.
    readOneAuthority(authority_id): Observable<Authority> {
        return this._http
            .get('http://localhost/api/authority/read_one.php?id=' + authority_id)
            .pipe(map((res: Response) => res.json()));
    }

    // Send authority data to remote server to update it.
    updateAuthority(authority): Observable<Authority> {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/authority/update.php',
            authority,
            options
        ).pipe(map((res: Response) => res.json()));
    }

    // Send authority ID to remote server to delete it.
    deleteAuthority(authority_id) {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/authority/delete.php',
            { id: authority_id },
            options
        ).pipe(map((res: Response) => res.json()));
    }

}
