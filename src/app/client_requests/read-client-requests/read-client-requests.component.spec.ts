import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadClientRequestsComponent } from './read-client-requests.component';

describe('ReadClientRequestsComponent', () => {
  let component: ReadClientRequestsComponent;
  let fixture: ComponentFixture<ReadClientRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadClientRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadClientRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
