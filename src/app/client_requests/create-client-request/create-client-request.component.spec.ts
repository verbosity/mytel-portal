import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateClientRequestComponent } from './create-client-request.component';

describe('CreateClientRequestComponent', () => {
  let component: CreateClientRequestComponent;
  let fixture: ComponentFixture<CreateClientRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateClientRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateClientRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
