import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateClientRequestComponent } from './update-client-request.component';

describe('UpdateClientRequestComponent', () => {
  let component: UpdateClientRequestComponent;
  let fixture: ComponentFixture<UpdateClientRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateClientRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateClientRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
