import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadOneClientRequestComponent } from './read-one-client-request.component';

describe('ReadOneClientRequestComponent', () => {
  let component: ReadOneClientRequestComponent;
  let fixture: ComponentFixture<ReadOneClientRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadOneClientRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadOneClientRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
