import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteClientRequestComponent } from './delete-client-request.component';

describe('DeleteClientRequestComponent', () => {
  let component: DeleteClientRequestComponent;
  let fixture: ComponentFixture<DeleteClientRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteClientRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteClientRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
