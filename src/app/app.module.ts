import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
 
import { AppComponent } from './app.component';
import { ReadProductsComponent } from './products/read-products/read-products.component';
import { CreateProductComponent } from './products/create-product/create-product.component';
import { ReadOneProductComponent } from './products/read-one-product/read-one-product.component';
import { UpdateProductComponent } from './products/update-product/update-product.component';
import { DeleteProductComponent } from './products/delete-product/delete-product.component';
import { ReadPlatformsComponent } from './platforms/read-platforms/read-platforms.component';
import { ReadPackagesComponent } from './packages/read-packages/read-packages.component';
import { ReadTokensComponent } from './tokens/read-tokens/read-tokens.component';
import { ReadUsersComponent } from './users/read-users/read-users.component';
import { ReadAuthoritiesComponent } from './authorities/read-authorities/read-authorities.component';
import { ReadManufacturersComponent } from './manufacturers/read-manufacturers/read-manufacturers.component';
import { ReadSuppliersComponent } from './suppliers/read-suppliers/read-suppliers.component';
import { ReadDevicesComponent } from './devices/read-devices/read-devices.component';
import { ReadClientsComponent } from './clients/read-clients/read-clients.component';
import { ReadClientRequestsComponent } from './client_requests/read-client-requests/read-client-requests.component';
import { ReadClientTokensComponent } from './client_tokens/read-client-tokens/read-client-tokens.component';
import { ReadClientUsernumbersComponent } from './client_usernumbers/read-client-usernumbers/read-client-usernumbers.component';
import { ReadClientDevicesComponent } from './client_devices/read-client-devices/read-client-devices.component';
import { ReadOnePackageComponent } from './packages/read-one-package/read-one-package.component';
import { CreatePackageComponent } from './packages/create-package/create-package.component';
import { UpdatePackageComponent } from './packages/update-package/update-package.component';
import { DeletePackageComponent } from './packages/delete-package/delete-package.component';
import { ReadOnePlatformComponent } from './platforms/read-one-platform/read-one-platform.component';
import { CreatePlatformComponent } from './platforms/create-platform/create-platform.component';
import { UpdatePlatformComponent } from './platforms/update-platform/update-platform.component';
import { DeletePlatformComponent } from './platforms/delete-platform/delete-platform.component';
import { ReadOneTokenComponent } from './tokens/read-one-token/read-one-token.component';
import { CreateTokenComponent } from './tokens/create-token/create-token.component';
import { UpdateTokenComponent } from './tokens/update-token/update-token.component';
import { DeleteTokenComponent } from './tokens/delete-token/delete-token.component';
import { ReadOneUserComponent } from './users/read-one-user/read-one-user.component';
import { CreateUserComponent } from './users/create-user/create-user.component';
import { UpdateUserComponent } from './users/update-user/update-user.component';
import { DeleteUserComponent } from './users/delete-user/delete-user.component';
import { ReadOneAuthorityComponent } from './authorities/read-one-authority/read-one-authority.component';
import { CreateAuthorityComponent } from './authorities/create-authority/create-authority.component';
import { UpdateAuthorityComponent } from './authorities/update-authority/update-authority.component';
import { DeleteAuthorityComponent } from './authorities/delete-authority/delete-authority.component';
import { ReadOneManufacturerComponent } from './manufacturers/read-one-manufacturer/read-one-manufacturer.component';
import { CreateManufacturerComponent } from './manufacturers/create-manufacturer/create-manufacturer.component';
import { UpdateManufacturerComponent } from './manufacturers/update-manufacturer/update-manufacturer.component';
import { DeleteManufacturerComponent } from './manufacturers/delete-manufacturer/delete-manufacturer.component';
import { ReadOneSupplierComponent } from './suppliers/read-one-supplier/read-one-supplier.component';
import { CreateSupplierComponent } from './suppliers/create-supplier/create-supplier.component';
import { UpdateSupplierComponent } from './suppliers/update-supplier/update-supplier.component';
import { DeleteSupplierComponent } from './suppliers/delete-supplier/delete-supplier.component';
import { ReadOneDeviceComponent } from './devices/read-one-device/read-one-device.component';
import { CreateDeviceComponent } from './devices/create-device/create-device.component';
import { UpdateDeviceComponent } from './devices/update-device/update-device.component';
import { DeleteDeviceComponent } from './devices/delete-device/delete-device.component';
import { ReadOneClientComponent } from './clients/read-one-client/read-one-client.component';
import { CreateClientComponent } from './clients/create-client/create-client.component';
import { UpdateClientComponent } from './clients/update-client/update-client.component';
import { DeleteClientComponent } from './clients/delete-client/delete-client.component';
import { ReadOneClientRequestComponent } from './client_requests/read-one-client-request/read-one-client-request.component';
import { CreateClientRequestComponent } from './client_requests/create-client-request/create-client-request.component';
import { UpdateClientRequestComponent } from './client_requests/update-client-request/update-client-request.component';
import { DeleteClientRequestComponent } from './client_requests/delete-client-request/delete-client-request.component';
import { ReadOneClientTokenComponent } from './client_tokens/read-one-client-token/read-one-client-token.component';
import { CreateClientTokenComponent } from './client_tokens/create-client-token/create-client-token.component';
import { UpdateClientTokenComponent } from './client_tokens/update-client-token/update-client-token.component';
import { DeleteClientTokenComponent } from './client_tokens/delete-client-token/delete-client-token.component';
import { ReadOneClientUsernumberComponent } from './client_usernumbers/read-one-client-usernumber/read-one-client-usernumber.component';
import { CreateClientUsernumberComponent } from './client_usernumbers/create-client-usernumber/create-client-usernumber.component';
import { UpdateClientUsernumberComponent } from './client_usernumbers/update-client-usernumber/update-client-usernumber.component';
import { DeleteClientUsernumberComponent } from './client_usernumbers/delete-client-usernumber/delete-client-usernumber.component';
import { ReadOneClientDeviceComponent } from './client_devices/read-one-client-device/read-one-client-device.component';
import { CreateClientDeviceComponent } from './client_devices/create-client-device/create-client-device.component';
import { UpdateClientDeviceComponent } from './client_devices/update-client-device/update-client-device.component';
import { DeleteClientDeviceComponent } from './client_devices/delete-client-device/delete-client-device.component';

 
@NgModule({
  declarations: [
    AppComponent,
    ReadProductsComponent,
    CreateProductComponent,
    ReadOneProductComponent,
    UpdateProductComponent,
    DeleteProductComponent,
    ReadPlatformsComponent,
    ReadPackagesComponent,
    ReadTokensComponent,
    ReadUsersComponent,
    ReadAuthoritiesComponent,
    ReadManufacturersComponent,
    ReadSuppliersComponent,
    ReadDevicesComponent,
    ReadClientsComponent,
    ReadClientRequestsComponent,
    ReadClientTokensComponent,
    ReadClientUsernumbersComponent,
    ReadClientDevicesComponent,
    ReadOnePackageComponent,
    CreatePackageComponent,
    UpdatePackageComponent,
    DeletePackageComponent,
    ReadOnePlatformComponent,
    CreatePlatformComponent,
    UpdatePlatformComponent,
    DeletePlatformComponent,
    ReadOneTokenComponent,
    CreateTokenComponent,
    UpdateTokenComponent,
    DeleteTokenComponent,
    ReadOneUserComponent,
    CreateUserComponent,
    UpdateUserComponent,
    DeleteUserComponent,
    ReadOneAuthorityComponent,
    CreateAuthorityComponent,
    UpdateAuthorityComponent,
    DeleteAuthorityComponent,
    ReadOneManufacturerComponent,
    CreateManufacturerComponent,
    UpdateManufacturerComponent,
    DeleteManufacturerComponent,
    ReadOneSupplierComponent,
    CreateSupplierComponent,
    UpdateSupplierComponent,
    DeleteSupplierComponent,
    ReadOneDeviceComponent,
    CreateDeviceComponent,
    UpdateDeviceComponent,
    DeleteDeviceComponent,
    ReadOneClientComponent,
    CreateClientComponent,
    UpdateClientComponent,
    DeleteClientComponent,
    ReadOneClientRequestComponent,
    CreateClientRequestComponent,
    UpdateClientRequestComponent,
    DeleteClientRequestComponent,
    ReadOneClientTokenComponent,
    CreateClientTokenComponent,
    UpdateClientTokenComponent,
    DeleteClientTokenComponent,
    ReadOneClientUsernumberComponent,
    CreateClientUsernumberComponent,
    UpdateClientUsernumberComponent,
    DeleteClientUsernumberComponent,
    ReadOneClientDeviceComponent,
    CreateClientDeviceComponent,
    UpdateClientDeviceComponent,
    DeleteClientDeviceComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule
],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }