export class Client {
    constructor(
        public id: number,
        public title: string,
        public firstname: string,
        public lastname: string,
        public organisation: string,
        public street_address: string,
        public suburb: string,
        public city: string,
        public country: string,
        public telephone: string,
        public mobile: string,
        public email_address: string,
        public gender: string,
        public birthdate: Date,
        public avatar: string,
        public date: Date,
        public active: number,
        public departure: Date
    ) { }
}
