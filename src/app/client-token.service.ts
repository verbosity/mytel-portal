import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ClientToken } from './client-token';
import { map } from 'rxjs/operators';

@Injectable()

// Service for authorities data.
export class ClientTokenService {

    // We need Http to talk to a remote server.
    constructor(private _http: Http) { }

    // Get list of authorities from remote server.
    readClientTokens(): Observable<ClientToken[]> {

        return this._http
            .get('http://localhost/api/client_token/read.php')
            .pipe(map((res: Response) => res.json()));
    }

    // Send client_token data to remote server to create it.
    createClientToken(client_token): Observable<ClientToken> {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/client_token/create.php',
            client_token,
            options
        ).pipe(map((res: Response) => res.json()));
    }

    // Get a client_token details from remote server.
    readOneClientToken(client_token_id): Observable<ClientToken> {
        return this._http
            .get('http://localhost/api/client_token/read_one.php?id=' + client_token_id)
            .pipe(map((res: Response) => res.json()));
    }

    // Send client_token data to remote server to update it.
    updateClientToken(client_token): Observable<ClientToken> {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/client_token/update.php',
            client_token,
            options
        ).pipe(map((res: Response) => res.json()));
    }

    // Send client_token ID to remote server to delete it.
    deleteClientToken(client_token_id) {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/client_token/delete.php',
            { id: client_token_id },
            options
        ).pipe(map((res: Response) => res.json()));
    }

}
