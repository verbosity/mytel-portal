import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Category } from './category';
import { map } from 'rxjs/operators';

@Injectable()

// Service for categorys data.
export class CategoryService {

    // We need Http to talk to a remote server.
    constructor(private _http: Http) { }

    // Get list of categorys from remote server.
    readCategories(): Observable<Category[]> {

        return this._http
            .get('http://localhost/api/category/read.php')
            .pipe(map((res: Response) => res.json()));
    }

    // Send category data to remote server to create it.
    createCategory(category): Observable<Category> {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/category/create.php',
            category,
            options
        ).pipe(map((res: Response) => res.json()));
    }

    // Get a category details from remote server.
    readOneCategory(category_id): Observable<Category> {
        return this._http
            .get('http://localhost/api/category/read_one.php?id=' + category_id)
            .pipe(map((res: Response) => res.json()));
    }

    // Send category data to remote server to update it.
    updateCategory(category): Observable<Category> {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/category/update.php',
            category,
            options
        ).pipe(map((res: Response) => res.json()));
    }

    // Send category ID to remote server to delete it.
    deleteCategory(category_id) {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/category/delete.php',
            { id: category_id },
            options
        ).pipe(map((res: Response) => res.json()));
    }

}
