export class Package {
    constructor(
        public id: number,
        public package_name: string,
        public value: number,
        public bandwidth: number,
        public datarate: number,
        public platform_id: number,
        public platform: string,
        public active: number
    ) { }
}
