import { TestBed, inject } from '@angular/core/testing';

import { ClientDeviceService } from './client-device.service';

describe('ClientDeviceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClientDeviceService]
    });
  });

  it('should be created', inject([ClientDeviceService], (service: ClientDeviceService) => {
    expect(service).toBeTruthy();
  }));
});
