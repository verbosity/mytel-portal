import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Device } from './device';
import { map } from 'rxjs/operators';

@Injectable()

// Service for authorities data.
export class DeviceService {

    // We need Http to talk to a remote server.
    constructor(private _http: Http) { }

    // Get list of authorities from remote server.
    readDevices(): Observable<Device[]> {

        return this._http
            .get('http://localhost/api/device/read.php')
            .pipe(map((res: Response) => res.json()));
    }

    // Send device data to remote server to create it.
    createDevice(device): Observable<Device> {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/device/create.php',
            device,
            options
        ).pipe(map((res: Response) => res.json()));
    }

    // Get a device details from remote server.
    readOneDevice(device_id): Observable<Device> {
        return this._http
            .get('http://localhost/api/device/read_one.php?id=' + device_id)
            .pipe(map((res: Response) => res.json()));
    }

    // Send device data to remote server to update it.
    updateDevice(device): Observable<Device> {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/device/update.php',
            device,
            options
        ).pipe(map((res: Response) => res.json()));
    }

    // Send device ID to remote server to delete it.
    deleteDevice(device_id) {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/device/delete.php',
            { id: device_id },
            options
        ).pipe(map((res: Response) => res.json()));
    }

}
