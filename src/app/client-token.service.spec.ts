import { TestBed, inject } from '@angular/core/testing';

import { ClientTokenService } from './client-token.service';

describe('ClientTokenService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClientTokenService]
    });
  });

  it('should be created', inject([ClientTokenService], (service: ClientTokenService) => {
    expect(service).toBeTruthy();
  }));
});
