import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadOneDeviceComponent } from './read-one-device.component';

describe('ReadOneDeviceComponent', () => {
  let component: ReadOneDeviceComponent;
  let fixture: ComponentFixture<ReadOneDeviceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadOneDeviceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadOneDeviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
