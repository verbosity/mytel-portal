import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadDevicesComponent } from './read-devices.component';

describe('ReadDevicesComponent', () => {
  let component: ReadDevicesComponent;
  let fixture: ComponentFixture<ReadDevicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadDevicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadDevicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
