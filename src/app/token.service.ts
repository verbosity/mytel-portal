import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Token } from './token';
import { map } from 'rxjs/operators';

@Injectable()

// Service for authorities data.
export class TokenService {

    // We need Http to talk to a remote server.
    constructor(private _http: Http) { }

    // Get list of authorities from remote server.
    readTokens(): Observable<Token[]> {

        return this._http
            .get('http://localhost/api/token/read.php')
            .pipe(map((res: Response) => res.json()));
    }

    // Send token data to remote server to create it.
    createToken(token): Observable<Token> {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/token/create.php',
            token,
            options
        ).pipe(map((res: Response) => res.json()));
    }

    // Get a token details from remote server.
    readOneToken(token_id): Observable<Token> {
        return this._http
            .get('http://localhost/api/token/read_one.php?id=' + token_id)
            .pipe(map((res: Response) => res.json()));
    }

    // Send token data to remote server to update it.
    updateToken(token): Observable<Token> {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/token/update.php',
            token,
            options
        ).pipe(map((res: Response) => res.json()));
    }

    // Send token ID to remote server to delete it.
    deleteToken(token_id) {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/token/delete.php',
            { id: token_id },
            options
        ).pipe(map((res: Response) => res.json()));
    }

}
