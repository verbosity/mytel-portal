import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Package } from './package';
import { map } from 'rxjs/operators';

@Injectable()

// Service for authorities data.
export class PackageService {

    // We need Http to talk to a remote server.
    constructor(private _http: Http) { }

    // Get list of authorities from remote server.
    readPackages(): Observable<Package[]> {

        return this._http
            .get('http://localhost/api/package/read.php')
            .pipe(map((res: Response) => res.json()));
    }

    // Send package data to remote server to create it.
    createPackage(package_o): Observable<Package> {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/package/create.php',
            package_o,
            options
        ).pipe(map((res: Response) => res.json()));
    }

    // Get a package details from remote server.
    readOnePackage(package_id): Observable<Package> {
        return this._http
            .get('http://localhost/api/package/read_one.php?id=' + package_id)
            .pipe(map((res: Response) => res.json()));
    }

    // Send package data to remote server to update it.
    updatePackage(package_o): Observable<Package> {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/package/update.php',
            package_o,
            options
        ).pipe(map((res: Response) => res.json()));
    }

    // Send package ID to remote server to delete it.
    deletePackage(package_id) {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/package/delete.php',
            { id: package_id },
            options
        ).pipe(map((res: Response) => res.json()));
    }

}
