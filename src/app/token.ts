export class Token {
    constructor(
        public id: number,
        public package_id: number,
        public package_name: string,
        public token: string,
        public serial_number: string,
        public expiry: Date,
        public available: number,
        public date: Date
    ) { }
}
