export class Supplier {
    constructor(
        public id: number,
        public supplier: string,
        public contact: string,
        public street_address: string,
        public suburb: string,
        public city: string,
        public country: string,
        public telephone: string,
        public mobile: string,
        public email_address: string,
        public website: string
    ) { }
}
