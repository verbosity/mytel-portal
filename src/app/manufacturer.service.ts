import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Manufacturer } from './manufacturer';
import { map } from 'rxjs/operators';

@Injectable()

// Service for authorities data.
export class ManufacturerService {

    // We need Http to talk to a remote server.
    constructor(private _http: Http) { }

    // Get list of authorities from remote server.
    readManufacturers(): Observable<Manufacturer[]> {

        return this._http
            .get('http://localhost/api/manufacturer/read.php')
            .pipe(map((res: Response) => res.json()));
    }

    // Send manufacturer data to remote server to create it.
    createManufacturer(manufacturer): Observable<Manufacturer> {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/manufacturer/create.php',
            manufacturer,
            options
        ).pipe(map((res: Response) => res.json()));
    }

    // Get a manufacturer details from remote server.
    readOneManufacturer(manufacturer_id): Observable<Manufacturer> {
        return this._http
            .get('http://localhost/api/manufacturer/read_one.php?id=' + manufacturer_id)
            .pipe(map((res: Response) => res.json()));
    }

    // Send manufacturer data to remote server to update it.
    updateManufacturer(manufacturer): Observable<Manufacturer> {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/manufacturer/update.php',
            manufacturer,
            options
        ).pipe(map((res: Response) => res.json()));
    }

    // Send manufacturer ID to remote server to delete it.
    deleteManufacturer(manufacturer_id) {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/manufacturer/delete.php',
            { id: manufacturer_id },
            options
        ).pipe(map((res: Response) => res.json()));
    }

}
