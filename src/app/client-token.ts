export class ClientToken {
    constructor(
        public id: number,
        public client_usernumber_id: number,
        public client_usernumber: string,
        public token_id: number,
        public token: string,
        public client_request_id: number,
        public used: number,
        public remaining: number,
        public expiry: Date,
        public active: number,
        public date: Date
    ) { }
}
