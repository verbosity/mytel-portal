export class Device {
    constructor(
        public id: number,
        public device: string,
        public specifications: string,
        public manufacturer_id: number,
        public manufacturer: string,
        public supplier_id: number,
        public supplier: string
    ) { }
}
