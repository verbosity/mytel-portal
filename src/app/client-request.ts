export class ClientRequest {
    constructor(
        public id: number,
        public client_usernumber_id: number,
        public client_usernumber: string,
        public package_id: number,
        public package_name: string,
        public payment_method: string,
        public value: number,
        public approved: number,
        public date: Date
    ) { }
}
