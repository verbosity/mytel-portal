export class User {
    constructor(
        public id: number,
        public username: string,
        public password: string,
        public authority_id: number,
        public fullname: string,
        public active: number
    ) { }
}
