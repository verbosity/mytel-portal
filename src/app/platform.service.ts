import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Platform } from './platform';
import { map } from 'rxjs/operators';

@Injectable()

// Service for authorities data.
export class PlatformService {

    // We need Http to talk to a remote server.
    constructor(private _http: Http) { }

    // Get list of authorities from remote server.
    readPlatforms(): Observable<Platform[]> {

        return this._http
            .get('http://localhost/api/platform/read.php')
            .pipe(map((res: Response) => res.json()));
    }

    // Send platform data to remote server to create it.
    createPlatform(platform): Observable<Platform> {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/platform/create.php',
            platform,
            options
        ).pipe(map((res: Response) => res.json()));
    }

    // Get a platform details from remote server.
    readOnePlatform(platform_id): Observable<Platform> {
        return this._http
            .get('http://localhost/api/platform/read_one.php?id=' + platform_id)
            .pipe(map((res: Response) => res.json()));
    }

    // Send platform data to remote server to update it.
    updatePlatform(platform): Observable<Platform> {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/platform/update.php',
            platform,
            options
        ).pipe(map((res: Response) => res.json()));
    }

    // Send platform ID to remote server to delete it.
    deletePlatform(platform_id) {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/platform/delete.php',
            { id: platform_id },
            options
        ).pipe(map((res: Response) => res.json()));
    }

}
