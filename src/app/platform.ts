export class Platform {
    constructor(
        public id: number,
        public platform: string,
        public active: number
    ) { }
}
