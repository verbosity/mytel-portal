import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ClientRequest } from './client-request';
import { map } from 'rxjs/operators';

@Injectable()

// Service for authorities data.
export class ClientRequestService {

    // We need Http to talk to a remote server.
    constructor(private _http: Http) { }

    // Get list of authorities from remote server.
    readClientRequests(): Observable<ClientRequest[]> {

        return this._http
            .get('http://localhost/api/client_request/read.php')
            .pipe(map((res: Response) => res.json()));
    }

    // Send client_request data to remote server to create it.
    createClientRequest(client_request): Observable<ClientRequest> {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/client_request/create.php',
            client_request,
            options
        ).pipe(map((res: Response) => res.json()));
    }

    // Get a client_request details from remote server.
    readOneClientRequest(client_request_id): Observable<ClientRequest> {
        return this._http
            .get('http://localhost/api/client_request/read_one.php?id=' + client_request_id)
            .pipe(map((res: Response) => res.json()));
    }

    // Send client_request data to remote server to update it.
    updateClientRequest(client_request): Observable<ClientRequest> {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/client_request/update.php',
            client_request,
            options
        ).pipe(map((res: Response) => res.json()));
    }

    // Send client_request ID to remote server to delete it.
    deleteClientRequest(client_request_id) {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/client_request/delete.php',
            { id: client_request_id },
            options
        ).pipe(map((res: Response) => res.json()));
    }

}
