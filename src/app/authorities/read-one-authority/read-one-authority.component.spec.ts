import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadOneAuthorityComponent } from './read-one-authority.component';

describe('ReadOneAuthorityComponent', () => {
  let component: ReadOneAuthorityComponent;
  let fixture: ComponentFixture<ReadOneAuthorityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadOneAuthorityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadOneAuthorityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
