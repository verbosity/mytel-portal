import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadAuthoritiesComponent } from './read-authorities.component';

describe('ReadAuthoritiesComponent', () => {
  let component: ReadAuthoritiesComponent;
  let fixture: ComponentFixture<ReadAuthoritiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadAuthoritiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadAuthoritiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
