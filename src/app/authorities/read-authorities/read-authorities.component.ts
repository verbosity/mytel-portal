import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthorityService } from '../../authority.service';
import { Observable } from 'rxjs';
import { Authority } from '../../authority';

@Component({
    selector: 'app-read-authorities',
    templateUrl: './read-authorities.component.html',
    styleUrls: ['./read-authorities.component.css'],
    providers: [AuthorityService]
})

export class ReadAuthoritysComponent implements OnInit {

    @Output() show_create_authority_event = new EventEmitter();
    @Output() show_read_one_authority_event = new EventEmitter();
    @Output() show_update_authority_event = new EventEmitter();
    @Output() show_delete_authority_event = new EventEmitter();

    // store list of authorities
    authorities: Authority[];

    // initialize authorityService to retrieve list authorities in the ngOnInit()
    constructor(private authorityService: AuthorityService) { }

    // when user clicks the 'create' button
    createAuthority() {
        // tell the parent component (AppComponent)
        this.show_create_authority_event.emit({
            title: 'Create authority'
        });
    }

    // when user clicks the 'read' button
    readOneAuthority(id) {
        console.log('rp comp readOneAuthority');
        // tell the parent component (AppComponent)
        this.show_read_one_authority_event.emit({
            authority_id: id,
            title: 'Read One authority'
        });
    }

    // when user clicks the 'update' button
    updateAuthority(id) {
        // tell the parent component (AppComponent)
        this.show_update_authority_event.emit({
            authority_id: id,
            title: 'Update authority'
        });
    }

    // when user clicks the 'delete' button
    deleteAuthority(id) {
        // tell the parent component (AppComponent)
        this.show_delete_authority_event.emit({
            authority_id: id,
            title: 'Delete authority'
        });
    }

    // Read authorities from API.
    ngOnInit() {
        this.authorityService.readAuthorities()
            .subscribe(authorities =>
                this.authorities = authorities['records']
            );
    }
}
