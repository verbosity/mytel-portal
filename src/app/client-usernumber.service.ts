import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ClientUsernumber } from './client-usernumber';
import { map } from 'rxjs/operators';

@Injectable()

// Service for authorities data.
export class ClientUsernumberService {

    // We need Http to talk to a remote server.
    constructor(private _http: Http) { }

    // Get list of authorities from remote server.
    readClientUsernumbers(): Observable<ClientUsernumber[]> {

        return this._http
            .get('http://localhost/api/client_usernumber/read.php')
            .pipe(map((res: Response) => res.json()));
    }

    // Send client_usernumber data to remote server to create it.
    createClientUsernumber(client_usernumber): Observable<ClientUsernumber> {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/client_usernumber/create.php',
            client_usernumber,
            options
        ).pipe(map((res: Response) => res.json()));
    }

    // Get a client_usernumber details from remote server.
    readOneClientUsernumber(client_usernumber_id): Observable<ClientUsernumber> {
        return this._http
            .get('http://localhost/api/client_usernumber/read_one.php?id=' + client_usernumber_id)
            .pipe(map((res: Response) => res.json()));
    }

    // Send client_usernumber data to remote server to update it.
    updateClientUsernumber(client_usernumber): Observable<ClientUsernumber> {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/client_usernumber/update.php',
            client_usernumber,
            options
        ).pipe(map((res: Response) => res.json()));
    }

    // Send client_usernumber ID to remote server to delete it.
    deleteClientUsernumber(client_usernumber_id) {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/client_usernumber/delete.php',
            { id: client_usernumber_id },
            options
        ).pipe(map((res: Response) => res.json()));
    }

}
