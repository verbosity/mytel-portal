import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadOnePackageComponent } from './read-one-package.component';

describe('ReadOnePackageComponent', () => {
  let component: ReadOnePackageComponent;
  let fixture: ComponentFixture<ReadOnePackageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadOnePackageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadOnePackageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
