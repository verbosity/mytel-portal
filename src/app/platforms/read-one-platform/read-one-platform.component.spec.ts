import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadOnePlatformComponent } from './read-one-platform.component';

describe('ReadOnePlatformComponent', () => {
  let component: ReadOnePlatformComponent;
  let fixture: ComponentFixture<ReadOnePlatformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadOnePlatformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadOnePlatformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
