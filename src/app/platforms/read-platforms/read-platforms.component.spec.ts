import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadPlatformsComponent } from './read-platforms.component';

describe('ReadPlatformsComponent', () => {
  let component: ReadPlatformsComponent;
  let fixture: ComponentFixture<ReadPlatformsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadPlatformsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadPlatformsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
