import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeletePlatformComponent } from './delete-platform.component';

describe('DeletePlatformComponent', () => {
  let component: DeletePlatformComponent;
  let fixture: ComponentFixture<DeletePlatformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeletePlatformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeletePlatformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
