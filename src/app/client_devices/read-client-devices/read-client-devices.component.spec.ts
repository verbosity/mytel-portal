import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadClientDevicesComponent } from './read-client-devices.component';

describe('ReadClientDevicesComponent', () => {
  let component: ReadClientDevicesComponent;
  let fixture: ComponentFixture<ReadClientDevicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadClientDevicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadClientDevicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
