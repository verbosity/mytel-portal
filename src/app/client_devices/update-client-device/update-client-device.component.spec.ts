import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateClientDeviceComponent } from './update-client-device.component';

describe('UpdateClientDeviceComponent', () => {
  let component: UpdateClientDeviceComponent;
  let fixture: ComponentFixture<UpdateClientDeviceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateClientDeviceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateClientDeviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
