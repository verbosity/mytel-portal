import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadOneClientDeviceComponent } from './read-one-client-device.component';

describe('ReadOneClientDeviceComponent', () => {
  let component: ReadOneClientDeviceComponent;
  let fixture: ComponentFixture<ReadOneClientDeviceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadOneClientDeviceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadOneClientDeviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
