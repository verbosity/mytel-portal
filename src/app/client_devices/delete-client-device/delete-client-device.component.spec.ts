import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteClientDeviceComponent } from './delete-client-device.component';

describe('DeleteClientDeviceComponent', () => {
  let component: DeleteClientDeviceComponent;
  let fixture: ComponentFixture<DeleteClientDeviceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteClientDeviceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteClientDeviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
