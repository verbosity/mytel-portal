import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateClientDeviceComponent } from './create-client-device.component';

describe('CreateClientDeviceComponent', () => {
  let component: CreateClientDeviceComponent;
  let fixture: ComponentFixture<CreateClientDeviceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateClientDeviceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateClientDeviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
