import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Client } from './client';
import { map } from 'rxjs/operators';

@Injectable()

// Service for authorities data.
export class ClientService {

    // We need Http to talk to a remote server.
    constructor(private _http: Http) { }

    // Get list of authorities from remote server.
    readClients(): Observable<Client[]> {

        return this._http
            .get('http://localhost/api/client/read.php')
            .pipe(map((res: Response) => res.json()));
    }

    // Send client data to remote server to create it.
    createClient(client): Observable<Client> {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/client/create.php',
            client,
            options
        ).pipe(map((res: Response) => res.json()));
    }

    // Get a client details from remote server.
    readOneClient(client_id): Observable<Client> {
        return this._http
            .get('http://localhost/api/client/read_one.php?id=' + client_id)
            .pipe(map((res: Response) => res.json()));
    }

    // Send client data to remote server to update it.
    updateClient(client): Observable<Client> {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/client/update.php',
            client,
            options
        ).pipe(map((res: Response) => res.json()));
    }

    // Send client ID to remote server to delete it.
    deleteClient(client_id) {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/client/delete.php',
            { id: client_id },
            options
        ).pipe(map((res: Response) => res.json()));
    }

}
