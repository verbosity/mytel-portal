import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ClientDevice } from './client-device';
import { map } from 'rxjs/operators';

@Injectable()

// Service for authorities data.
export class ClientDeviceService {

    // We need Http to talk to a remote server.
    constructor(private _http: Http) { }

    // Get list of authorities from remote server.
    readClientDevices(): Observable<ClientDevice[]> {

        return this._http
            .get('http://localhost/api/client_device/read.php')
            .pipe(map((res: Response) => res.json()));
    }

    // Send client_device data to remote server to create it.
    createClientDevice(client_device): Observable<ClientDevice> {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/client_device/create.php',
            client_device,
            options
        ).pipe(map((res: Response) => res.json()));
    }

    // Get a client_device details from remote server.
    readOneClientDevice(client_device_id): Observable<ClientDevice> {
        return this._http
            .get('http://localhost/api/client_device/read_one.php?id=' + client_device_id)
            .pipe(map((res: Response) => res.json()));
    }

    // Send client_device data to remote server to update it.
    updateClientDevice(client_device): Observable<ClientDevice> {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/client_device/update.php',
            client_device,
            options
        ).pipe(map((res: Response) => res.json()));
    }

    // Send client_device ID to remote server to delete it.
    deleteClientDevice(client_device_id) {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/client_device/delete.php',
            { id: client_device_id },
            options
        ).pipe(map((res: Response) => res.json()));
    }

}
