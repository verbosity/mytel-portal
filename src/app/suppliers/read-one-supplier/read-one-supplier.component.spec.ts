import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadOneSupplierComponent } from './read-one-supplier.component';

describe('ReadOneSupplierComponent', () => {
  let component: ReadOneSupplierComponent;
  let fixture: ComponentFixture<ReadOneSupplierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadOneSupplierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadOneSupplierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
