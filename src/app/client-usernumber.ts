export class ClientUsernumber {
    constructor(
        public id: number,
        public client_id: number,
        public client: string,
        public usernumber: string,
        public password: string,
        public title: string,
        public street_address: string,
        public suburb: string,
        public city: string,
        public country: string,
        public telephone: string,
        public mobile: string,
        public active: number
    ) { }
}
