import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadOneManufacturerComponent } from './read-one-manufacturer.component';

describe('ReadOneManufacturerComponent', () => {
  let component: ReadOneManufacturerComponent;
  let fixture: ComponentFixture<ReadOneManufacturerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadOneManufacturerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadOneManufacturerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
