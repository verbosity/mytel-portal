import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadManufacturersComponent } from './read-manufacturers.component';

describe('ReadManufacturersComponent', () => {
  let component: ReadManufacturersComponent;
  let fixture: ComponentFixture<ReadManufacturersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadManufacturersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadManufacturersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
