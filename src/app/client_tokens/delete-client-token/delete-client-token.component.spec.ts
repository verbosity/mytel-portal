import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteClientTokenComponent } from './delete-client-token.component';

describe('DeleteClientTokenComponent', () => {
  let component: DeleteClientTokenComponent;
  let fixture: ComponentFixture<DeleteClientTokenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteClientTokenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteClientTokenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
