import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateClientTokenComponent } from './create-client-token.component';

describe('CreateClientTokenComponent', () => {
  let component: CreateClientTokenComponent;
  let fixture: ComponentFixture<CreateClientTokenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateClientTokenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateClientTokenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
