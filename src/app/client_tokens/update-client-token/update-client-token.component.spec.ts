import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateClientTokenComponent } from './update-client-token.component';

describe('UpdateClientTokenComponent', () => {
  let component: UpdateClientTokenComponent;
  let fixture: ComponentFixture<UpdateClientTokenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateClientTokenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateClientTokenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
