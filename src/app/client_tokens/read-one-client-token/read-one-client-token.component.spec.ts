import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadOneClientTokenComponent } from './read-one-client-token.component';

describe('ReadOneClientTokenComponent', () => {
  let component: ReadOneClientTokenComponent;
  let fixture: ComponentFixture<ReadOneClientTokenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadOneClientTokenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadOneClientTokenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
