import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadClientTokensComponent } from './read-client-tokens.component';

describe('ReadClientTokensComponent', () => {
  let component: ReadClientTokensComponent;
  let fixture: ComponentFixture<ReadClientTokensComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadClientTokensComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadClientTokensComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
