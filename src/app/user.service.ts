import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { User } from './user';
import { map } from 'rxjs/operators';

@Injectable()

// Service for authorities data.
export class UserService {

    // We need Http to talk to a remote server.
    constructor(private _http: Http) { }

    // Get list of authorities from remote server.
    readUsers(): Observable<User[]> {

        return this._http
            .get('http://localhost/api/user/read.php')
            .pipe(map((res: Response) => res.json()));
    }

    // Send user data to remote server to create it.
    createUser(user): Observable<User> {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/user/create.php',
            user,
            options
        ).pipe(map((res: Response) => res.json()));
    }

    // Get a user details from remote server.
    readOneUser(user_id): Observable<User> {
        return this._http
            .get('http://localhost/api/user/read_one.php?id=' + user_id)
            .pipe(map((res: Response) => res.json()));
    }

    // Send user data to remote server to update it.
    updateUser(user): Observable<User> {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/user/update.php',
            user,
            options
        ).pipe(map((res: Response) => res.json()));
    }

    // Send user ID to remote server to delete it.
    deleteUser(user_id) {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/user/delete.php',
            { id: user_id },
            options
        ).pipe(map((res: Response) => res.json()));
    }

}
