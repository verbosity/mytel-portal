import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadOneClientComponent } from './read-one-client.component';

describe('ReadOneClientComponent', () => {
  let component: ReadOneClientComponent;
  let fixture: ComponentFixture<ReadOneClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadOneClientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadOneClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
