import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Supplier } from './supplier';
import { map } from 'rxjs/operators';

@Injectable()

// Service for authorities data.
export class SupplierService {

    // We need Http to talk to a remote server.
    constructor(private _http: Http) { }

    // Get list of authorities from remote server.
    readSuppliers(): Observable<Supplier[]> {

        return this._http
            .get('http://localhost/api/supplier/read.php')
            .pipe(map((res: Response) => res.json()));
    }

    // Send supplier data to remote server to create it.
    createSupplier(supplier): Observable<Supplier> {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/supplier/create.php',
            supplier,
            options
        ).pipe(map((res: Response) => res.json()));
    }

    // Get a supplier details from remote server.
    readOneSupplier(supplier_id): Observable<Supplier> {
        return this._http
            .get('http://localhost/api/supplier/read_one.php?id=' + supplier_id)
            .pipe(map((res: Response) => res.json()));
    }

    // Send supplier data to remote server to update it.
    updateSupplier(supplier): Observable<Supplier> {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/supplier/update.php',
            supplier,
            options
        ).pipe(map((res: Response) => res.json()));
    }

    // Send supplier ID to remote server to delete it.
    deleteSupplier(supplier_id) {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(
            'http://localhost/api/supplier/delete.php',
            { id: supplier_id },
            options
        ).pipe(map((res: Response) => res.json()));
    }

}
