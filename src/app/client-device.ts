export class ClientDevice {
    constructor(
        public id: number,
        public client_usernumber_id: number,
        public client_usernumber: string,
        public device_id: number,
        public device: string,
        public condition: string,
        public date_installed: Date,
        public date_recalled: Date,
        public serial_number: string
    ) { }
}
