import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadOneClientUsernumberComponent } from './read-one-client-usernumber.component';

describe('ReadOneClientUsernumberComponent', () => {
  let component: ReadOneClientUsernumberComponent;
  let fixture: ComponentFixture<ReadOneClientUsernumberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadOneClientUsernumberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadOneClientUsernumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
