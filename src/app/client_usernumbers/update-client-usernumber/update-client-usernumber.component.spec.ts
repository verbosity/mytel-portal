import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateClientUsernumberComponent } from './update-client-usernumber.component';

describe('UpdateClientUsernumberComponent', () => {
  let component: UpdateClientUsernumberComponent;
  let fixture: ComponentFixture<UpdateClientUsernumberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateClientUsernumberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateClientUsernumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
