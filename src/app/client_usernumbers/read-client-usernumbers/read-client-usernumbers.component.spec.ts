import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadClientUsernumbersComponent } from './read-client-usernumbers.component';

describe('ReadClientUsernumbersComponent', () => {
  let component: ReadClientUsernumbersComponent;
  let fixture: ComponentFixture<ReadClientUsernumbersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadClientUsernumbersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadClientUsernumbersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
