import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteClientUsernumberComponent } from './delete-client-usernumber.component';

describe('DeleteClientUsernumberComponent', () => {
  let component: DeleteClientUsernumberComponent;
  let fixture: ComponentFixture<DeleteClientUsernumberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteClientUsernumberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteClientUsernumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
