import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateClientUsernumberComponent } from './create-client-usernumber.component';

describe('CreateClientUsernumberComponent', () => {
  let component: CreateClientUsernumberComponent;
  let fixture: ComponentFixture<CreateClientUsernumberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateClientUsernumberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateClientUsernumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
