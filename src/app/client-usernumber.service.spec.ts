import { TestBed, inject } from '@angular/core/testing';

import { ClientUsernumberService } from './client-usernumber.service';

describe('ClientUsernumberService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClientUsernumberService]
    });
  });

  it('should be created', inject([ClientUsernumberService], (service: ClientUsernumberService) => {
    expect(service).toBeTruthy();
  }));
});
