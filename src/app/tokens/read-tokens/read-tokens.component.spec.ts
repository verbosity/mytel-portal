import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadTokensComponent } from './read-tokens.component';

describe('ReadTokensComponent', () => {
  let component: ReadTokensComponent;
  let fixture: ComponentFixture<ReadTokensComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadTokensComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadTokensComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
