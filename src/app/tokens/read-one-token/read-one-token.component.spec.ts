import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadOneTokenComponent } from './read-one-token.component';

describe('ReadOneTokenComponent', () => {
  let component: ReadOneTokenComponent;
  let fixture: ComponentFixture<ReadOneTokenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadOneTokenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadOneTokenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
